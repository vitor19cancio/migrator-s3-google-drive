import boto3
import os
import sys
import json

from datetime import datetime
from io import BytesIO
from apiclient import discovery
from apiclient import errors
from apiclient.http import MediaIoBaseUpload
from googleapiclient.discovery import build


def get_service_account_credentials(credentials_parameter):
    from google.oauth2 import service_account
    import googleapiclient.discovery

    ssm_client = boto3.client('ssm')
    creds_dict = ssm_client.get_parameter(Name=credentials_parameter, WithDecryption=False)['Parameter']['Value']
    creds_json = json.loads(creds_dict)

    scopes_list = [
        'https://www.googleapis.com/auth/drive',
        'https://www.googleapis.com/auth/drive.file'
    ]

    credentials = service_account.Credentials.from_service_account_info(creds_json, scopes=scopes_list)

    return credentials


def upload_file(service, file_obj, file_name, description, source_folder, mime_type):
    CAPTURE_FOLDER_ID = '12ideWH9fVcO5cUqgWzLPlrXqC6DZ8C5D'
    VERIFY_FOLDER_ID = '1UpyADM01Z8Q2VzcIgOEAGtrelVDfzzFf'

    if not file_name:
        # Skip files with empty names
        return

    file_content = file_obj.read()
    media_body = MediaIoBaseUpload(BytesIO(file_content), mimetype=mime_type)

    if source_folder == 'capture':
        folder_id = CAPTURE_FOLDER_ID
    elif source_folder == 'verify':
        folder_id = VERIFY_FOLDER_ID
    else:
        # handle error case where source_folder is not 'capture' or 'verify'
        return

    body = {
        'name': file_name,
        'title': file_name,
        'description': description,
        'mimeType': mime_type,
        'parents': [folder_id]
    }

    file = service.files().create(
        supportsAllDrives=True,
        body=body,
        media_body=media_body).execute()

    print('{}, {}'.format(file_name, file['id']))

    return file


def main_handler(event, context):
    print('payload:', event)

    bucket = 'migrationpocbuckettriggerisetest'
    credentials_parameter = os.environ.get('GOOGLE_CREDENTIALS_PARAMETER')

    credentials = get_service_account_credentials(credentials_parameter)

    service = discovery.build('drive', 'v3', credentials=credentials, cache_discovery=False)

    s3_client = boto3.client('s3')
    paginator = s3_client.get_paginator('list_objects_v2')

    capture_prefix = 'capture/'
    verify_prefix = 'verify/'

    # get the current time
    current_time = datetime.now()

    for prefix in [capture_prefix, verify_prefix]:
        page_iterator = paginator.paginate(Bucket=bucket, Prefix=prefix)

        for page in page_iterator:
            if 'Contents' in page:
                for obj in page['Contents']:
                    key = obj['Key']
                    if key.endswith('/'):  # skip folder objects
                        continue

                    # get the last modified time of the file
                    last_modified_time = obj['LastModified'].replace(tzinfo=None)

                    # check if the file was modified in the last 5 minutes
                    if (current_time - last_modified_time).total_seconds() <= 300:
                        file_name = os.path.basename(key)

                        source_folder = 'capture' if prefix == capture_prefix else 'verify'
                        file_obj = s3_client.get_object(Bucket=bucket, Key=key)
                        upload_file(service, file_obj['Body'], file_name, file_name, source_folder,
                                    'application/octet-stream')

                        # s3_client.delete_object(Bucket=bucket, Key=key)
